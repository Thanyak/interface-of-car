package bcas.ap.inter.car;

public class BMW_M implements LuxuryCar {

	@Override
	public String carType() {
	
		return "Luxury Car";
	}

	@Override
	public String carBrand() {

		return "BMW";
	}

	@Override
	public String carModel() {

		return "BMW M135i.";
	}

	@Override
	public String getColor() {

		return "Black";
	}

	@Override
	public String getPrice() {

		return "LKR 22,000,000/- ";
	}

	@Override
	public String getSpeed() {

		return "0�100 km/h in 4.7 seconds";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {
	
		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
	
		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {
	
		return true;
	}


	@Override
	public boolean isRoofWindow() {

		return true;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}

	@Override
	public boolean isRemoteStart() {

		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return true;
	}


	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - The M135i is the hyper-hot version of the BMW 1 Series hatchback."+"\r\n"
				+ " - It has 18-inch wheels and lots of exclusive M features, like a rear spoiler, sports seats, improved braking, sports steering, sports suspension and exterior highlights. "
				+ "\r\n"+" - It has a 10.3-inch infotainment screen with Apple CarPlay.";
	}
}
