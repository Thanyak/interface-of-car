package bcas.ap.inter.car;

public class Audi implements LuxuryCar {

	@Override
	public String carType() {
	
		return "Luxury Car";
	}

	@Override
	public String carBrand() {
	
		return "Audi";
	}

	@Override
	public String carModel() {
		
		return "Audi A7 Sportback";
	}

	@Override
	public String getColor() {
	
		return "Blue";
	}

	@Override
	public String getPrice() {
	
		return "LKR 26,000,000/- ";
	}

	@Override
	public String getSpeed() {

		return "0-100 km/h (0-62.1 mph) in sec";
	}

	@Override
	public int getSeats() {
	
		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {
	
		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {
	
		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}

	@Override
	public boolean isRoofWindow() {

		return true;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {
	
		return true;
	}

	@Override
	public boolean isRemoteStart() {
	
		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {
	
		return true;
	}



	@Override
	public String getadditionalfeatures() {
	
		return "\r\n"+" - The four-door coup� is full of innovations � in terms of networking and digitalization."+"\r\n"+" - the sporty-confident driving experience,"
				+ " - as well as its versatile space concept.";
	}

	
}
