package bcas.ap.inter.car;

public class SuzukiAlto implements NormalCar{

	@Override
	public String carType() {

		return "Normal Car";
	}

	@Override
	public String carBrand() {

		return "Suzuki";
	}

	@Override
	public String carModel() {

		return "Suzuki Alto k10";
	}

	@Override
	public String getColor() {

		return "Sakhir Orange";
	}

	@Override
	public String getPrice() {

		return "Rs. 1,150,000";
	}

	@Override
	public String getSpeed() {
	
		return "Acceleration (0-100 kmph):13.3 Seconds";
	}

	@Override
	public int getSeats() {
	
		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {
		
		return false;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return false;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return false;
	}

	@Override
	public boolean isPowerWindows() {
	
		return false;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {
	
		return false;
	}

	@Override
	public boolean isDualAirbags() {

		return false;
	}

	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - Anti-Theft Device"+"\r\n"+" - Front Seat Pockets"+"\r\n"+" - Co-driver adjustable seat"+"\r\n"+" - Centrally Mounted Fuel Tank";
	}

}
