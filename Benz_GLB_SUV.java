package bcas.ap.inter.car;

public class Benz_GLB_SUV implements LuxuryCar{

	@Override
	public String carType() {

		return "Luxury Car";
	}

	@Override
	public String carBrand() {
	
		return "Mercedes Benz";
	}

	@Override
	public String carModel() {
	
		return "Benz GLB SUV";
	}

	@Override
	public String getColor() {
	
		return "Digital White metallic";
	}

	@Override
	public String getPrice() {
	
		return " LKR 10.4 Million";
	}

	@Override
	public String getSpeed() {

		return "0 to 60 sprint in 6.9 seconds";
	}

	@Override
	public int getSeats() {
	
		return 5-7;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {
	
		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}


	@Override
	public boolean isRoofWindow() {
	
		return true;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}

	@Override
	public boolean isRemoteStart() {
	
		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {
		
		return true;
	}

	@Override
	public String getadditionalfeatures() {
	
		return "\r\n"+" - Mercedes-Benz Emergency Call service"+"\r\n"+" - ATTENTION ASSIST"+"\r\n"+" - Dashcam"+"\r\n"+" - Power front seats with 3-position memory"+
		"\r\n"+" - Sport front seats"+"\r\n"+" - 40/20/40-split folding 2nd-row seats"+"\r\n"+" - 8G-DCT 8-speed dual-clutch automatic transmission"+"\r\n"+" - ECO Start/Stop system";
	}
}
