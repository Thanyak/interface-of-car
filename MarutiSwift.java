package bcas.ap.inter.car;

public class MarutiSwift implements NormalCar {

	@Override
	public String carType() {

		return "Normal Car";
	}

	@Override
	public String carBrand() {

		return "Maruthi";
	}

	@Override
	public String carModel() {

		return "Maruti Swift";
	}

	@Override
	public String getColor() {

		return "Red";
	}

	@Override
	public String getPrice() {

		return "Rs. 3,050,000";
	}

	@Override
	public String getSpeed() {

		return " Top speed of 160kmph and takes 16 seconds to reach 100kmph";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}

	@Override
	public String getadditionalfeatures() {
	
		return "\r\n"+" - Rear Parcel Shelf"+"\r\n"+" - Headlamp On Reminder"+"\r\n"+" - Gear Position Indicator"+"\r\n"+" - Driver Side Foot Rest"+"\r\n"+" - Navigation System"+"\r\n"+" - Anti lock breaking system"+"\r\n"+" - Power steering";
	}

}
