package bcas.ap.inter.car;

public class MercedesBenz implements LuxuryCar {


	@Override
	public String carType() {

		return "Luxary Car";
	}

	@Override
	public String carBrand() {

		return "MercedesBenz";
	}

	@Override
	public String carModel() {

		return "Mercedes-Benz S-Class";
	}

	@Override
	public String getColor() {

		return "Black";
	}

	@Override
	public String getPrice() {

		return "42.1 Million";
	}

	@Override
	public String getSpeed() {

		return "0-100kmph/4.6 sec";
	}

	@Override
	public int getSeats() {

		return 4;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}


	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}
	
	@Override
	public boolean isRoofWindow() {

		return false;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}

	@Override
	public boolean isRemoteStart() {

		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return true;
	}



	@Override
	public String getadditionalfeatures() {

		return 	"\r\n"+" - Two 12 V Sockets And Temperature Controlled Cup Holders."+"\r\n"+" - Futuristic Displays In HD Resolution."+ "\r\n"+
				" - Exclusive Trim Package Features Are Wood Trim In The Lower Area of The Doors In The Front And The Rear,"+"\r\n"+
				" - Reverse Of The Front Seat Backrests Faced In Wood Trim (Available Selectively)."+"\r\n"+
				" - Wood Trim Behind The Rear Bench Seat, Wood Covers On The Rear Air Vents."+ "\r\n"+" - Illuminated Door Sill Panels With MAYBACH Lettering Front And Rear."+"\r\n"+
				" - Air Balance package." +"\r\n"+" - Ambient Lighting With 64 Colours."+ "\r\n"+" - Analogue Clock.";
	}

}
