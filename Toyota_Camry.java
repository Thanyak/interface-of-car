package bcas.ap.inter.car;

public class Toyota_Camry implements NormalCar{

	@Override
	public String carType() {

		return "Normal Car";
	}

	@Override
	public String carBrand() {
		
		return "Toyota";
	}

	@Override
	public String carModel() {

		return "Toyota Camry";
	}

	@Override
	public String getColor() {

		return "Black";
	}

	@Override
	public String getPrice() {

		return "Rs. 3,490,000";
	}

	@Override
	public String getSpeed() {

		return "0-100kmph10.8 Seconds";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {
	
		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {
	
		return true;
	}

	@Override
	public boolean isAirConditioning() {
	
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
	
		return true;
	}

	@Override
	public boolean isABSsystem() {
		
		return true;
	}

	@Override
	public boolean isDualAirbags() {
	
		return true;
	}

	
	@Override
	public String getadditionalfeatures() {
	
		return "\r\n"+" - JBL Audio System: 9 Speakers with \"Clari-Fi\" TM Technology"+"\r\n"+" - The Toyota Camry has 1 Petrol Engine on offer."+
		"\r\n"+" - The Petrol engine is 2487 cc."+"\r\n"+" - It is available with the Automatic transmission."+
		"\r\n"+" - Depending upon the variant and fuel type the Camry has a mileage of 19.16 kmpl.";
	}
}
