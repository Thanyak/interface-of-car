package bcas.ap.inter.car;

public class BMW_Mini_Cooper implements LuxuryCar{

	@Override
	public String carType() {
	
		return "Luxury Car";
	}

	@Override
	public String carBrand() {
	
		return "BMW";
	}

	@Override
	public String carModel() {

		return "BMW Mini Cooper";
	}

	@Override
	public String getColor() {
	
		return "Black";
	}

	@Override
	public String getPrice() {

		return null;
	}

	@Override
	public String getSpeed() {

		return "Acceleration10.4 Seconds";
	}

	@Override
	public int getSeats() {
	
		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {
	
		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {
	
		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
	
		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return false;
	}

	@Override
	public boolean isRoofWindow() {
	
		return true;
	}

	@Override
	public boolean isbackupCamera() {
	
		return false;
	}

	@Override
	public boolean isNavigationSystem() {
	
		return true;
	}

	@Override
	public boolean isRemoteStart() {

		return false;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return false;
	}

	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - Side Impact Beams"+"\r\n"+" - Front Impact Beams"+"\r\n"+" - Traction Control"+"\r\n"+" - Adjustable Seats";
	}

}
