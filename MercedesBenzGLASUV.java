package bcas.ap.inter.car;

public class MercedesBenzGLASUV implements LuxuryCar{

	@Override
	public String carType() {

		return "Luxury Car";
	}

	@Override
	public String carBrand() {

		return "Mercedes Benz";
	}

	@Override
	public String carModel() {
	
		return "Mercedes Benz GLA-SUV";
	}

	@Override
	public String getColor() {

		return "Polar White";
	}

	@Override
	public String getPrice() {

		return "LKR 7,059,000";
	}

	@Override
	public String getSpeed() {

		return "Acceleration, 0-60 mph + 6.8 sec Disclaimer";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {
	
		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {
	
		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}


	@Override
	public boolean isRoofWindow() {

		return true;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}

	@Override
	public boolean isRemoteStart() {

		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return true;
	}
	
	
	@Override
	public String getadditionalfeatures() {
		// TODO Auto-generated method stub
		return "\r\n"+" - Automatic transmission"+"\r\n"+" - 2.0L inline-4 turbo Engine"+"\r\n"+" - 221 hp @ 5,500 rpm Power"+"\r\n"+" - 25 mpg Disclaimer City fuel economy";
	}
}
