package bcas.ap.inter.car;

public interface Car {
	public String carType();
	public String carBrand();
	public String carModel();
	public String getColor();
	public String getPrice();
	public String getSpeed();
	public int getSeats();
	public String isbodyConstruction();
	
}
