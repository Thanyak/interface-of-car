package bcas.ap.inter.car;

public class Toyota_86 implements NormalCar {

	@Override
	public String carType() {
		
		return "Normal Car";
	}

	@Override
	public String carBrand() {
	
		return "Toyota 86";
	}

	@Override
	public String carModel() {
	
		return "Toyota 86";
	}

	@Override
	public String getColor() {
	
		return "Red";
	}

	@Override
	public String getPrice() {

		return "5,089,500";
	}

	@Override
	public String getSpeed() {
		
		return " 0-100km/h in 7.6 seconds";
	}

	@Override
	public int getSeats() {

		return 4;
	}

	@Override
	public String isbodyConstruction() {
		
		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {
	
		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {
	
		return true;
	}

	@Override
	public boolean isAirConditioning() {
	
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {
	
		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
	
		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {
	
		return true;
	}

	@Override
	public String getadditionalfeatures() {

		return "\r\n"+"The second-generation Toyota GR 86 is due for the 2022 model year, and although it's completely new, it follows the same "+"\r\n"+"rear-wheel-drive formula as the first-gen car.";
	}

}
