package bcas.ap.inter.car;

public class Toyota implements NormalCar{

	@Override
	public String carType() {
	
		return "Normal Car";
	}

	@Override
	public String carBrand() {
	
		return "Toyoto";
	}

	@Override
	public String carModel() {

		return "Toyota Yaris";
	}

	@Override
	public String getColor() {
		
		return "Red";
	}

	@Override
	public String getPrice() {

		return "Rs. 4,075,000";
	}

	@Override
	public String getSpeed() {

		return "0-/ Seconds";
	}

	@Override
	public int getSeats() {
	
		return 5;
	}

	@Override
	public String isbodyConstruction() {
		
		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {
	
		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {
	
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
	
		return true;
	}

	@Override
	public boolean isABSsystem() {
	
		return true;
	}

	@Override
	public boolean isDualAirbags() {
	
		return true;
	}

	@Override
	public String getadditionalfeatures() {
		// TODO Auto-generated method stub
		return "\r\n"+" - 2 Tone Interiors with Waterfall Design Instrument Panel"+"\r\n"+" - Multi Information Display (MID)"+"\r\n"+" - Eco Indicator (4.2 Coloured TFT)"+"\r\n"+" - Optitron Meter";
	}

}
