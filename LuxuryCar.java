package bcas.ap.inter.car;

public interface LuxuryCar extends NormalCar {
	public boolean isRoofWindow();
	public boolean isbackupCamera();
	public boolean isNavigationSystem();
	public boolean isRemoteStart();
	public boolean isautomaticEmergencyBreaking();

}
