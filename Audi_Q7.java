package bcas.ap.inter.car;

public class Audi_Q7 implements LuxuryCar{

	@Override
	public String carType() {
	
		return "Luxury Car";
	}

	@Override
	public String carBrand() {

		return "Audi";
	}

	@Override
	public String carModel() {

		return "Audi Q7";
	}

	@Override
	public String getColor() {

		return "Galaxy Blue";
	}

	@Override
	public String getPrice() {

		return "Rs. 22,500,000";
	}

	@Override
	public String getSpeed() {

		return "0-100 km/h (sec):	6.5km/h";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}


	@Override
	public boolean isRoofWindow() {
	
		return true;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}

	@Override
	public boolean isRemoteStart() {
	
		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {
	
		return true;
	}
	
	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - Audi Virtual Cockpit"+"\r\n"+" - BOSE 3D Surround System"+"\r\n"+" - Surround View Camera"+"\r\n"+" - Rear Parking Sensors"+
		"\r\n"+" - Rear Parking Camera"+"\r\n"+" - Keyless Entry";
		
	}

}
