package bcas.ap.inter.car;

public class Audi_A8 implements LuxuryCar{

	@Override
	public String carType() {

		return "Luxury Car";
	}

	@Override
	public String carBrand() {

		return "Audi";
	}

	@Override
	public String carModel() {

		return "Audi.A8";
	}

	@Override
	public String getColor() {

		return "Gray";
	}

	@Override
	public String getPrice() {

		return "34.5 Million";
	}

	@Override
	public String getSpeed() {

		return "0-60 MPH : 5.6 sec";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {
	
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
	
		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}


	@Override
	public boolean isRoofWindow() {
	
		return true;
	}

	@Override
	public boolean isbackupCamera() {
	
		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}

	@Override
	public boolean isRemoteStart() {

		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return true;
	}
	
	@Override
	public String getadditionalfeatures() {
	
		return "\r\n"+" - The Audi A8 has 1 Petrol Engine on offer."+"\r\n"+" - The Petrol engine is 2995 cc"+"\r\n"+" - It is available with the Automatic transmission"+"\r\n"
		+" - Depending upon the variant and fuel type the A8 has a mileage of 11.7 kmpl."+"\r\n"+" - The A8 is a 5 seater and has length of 5302mm, width of 1945mm and a wheelbase of 3127mm";
	}

}
