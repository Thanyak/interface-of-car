package bcas.ap.inter.car;

public class BMW implements LuxuryCar{

	@Override
	public String carType() {
		
		return "Luxary Car";
	}

	@Override
	public String carBrand() {
	
		return "BMW";
	}

	@Override
	public String carModel() {
	
		return "BMW i8 Roadster";
	}

	@Override
	public String getColor() {
	
		return "AUSTIN YELLOW";
	}

	@Override
	public String getPrice() {
	
		return "LKR 17,500,000/-";
	}

	@Override
	public String getSpeed() {
	
		return "0 to 100 km/ h in just 4.6 seconds";
	}

	@Override
	public int getSeats() {
	
		return 4;
	}

	@Override
	public String isbodyConstruction() {
	
		return "good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}


	@Override
	public boolean isRoofWindow() {
	
		return true;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}



	@Override
	public boolean isRemoteStart() {

		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return true;
	}



	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - Fuel consumption in l/100 km (combined): 2.1"
				+ "\r\n"+" - Power consumption in kWh/100 km (combined): 14.5";
	}

}
