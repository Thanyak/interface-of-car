package bcas.ap.inter.car;

public class Suzuki implements NormalCar {

	@Override
	public String carType() {
	
		return "Normal Car";
	}

	@Override
	public String carBrand() {

		return "Suzuki";
	}

	@Override
	public String carModel() {

		return "Suzuki Celerio";
	}

	@Override
	public String getColor() {
	
		return "Blue";
	}

	@Override
	public String getPrice() {
	
		return "Rs. 3,175,000";
	}

	@Override
	public String getSpeed() {

		return "Acceleration (0-100 kmph)	15.05 Seconds";
	}

	@Override
	public int getSeats() {
	
		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {
	
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return false;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {
	
		return false;
	}

	@Override
	public boolean isDualAirbags() {
	
		return true;
	}

	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - Co-Dr Vanity Mirror in Sun Visor"+"\r\n"+" - Amber Illumination Colour"+"\r\n"+ " - 5 Drink Holder"+"\r\n"+" - Urethane Steering Wheel"+"\r\n"+" - Silver Painted Dial-Type Climate Control";
	}

}
