package bcas.ap.inter.car;

public class Maruti_Vitara implements NormalCar {

	@Override
	public String carType() {
	
		return "Normal Car";
	}

	@Override
	public String carBrand() {

		return "Maruthi";
	}

	@Override
	public String carModel() {
	
		return "Maruti_Vitara";
	}

	@Override
	public String getColor() {

		return "Red";
	}

	@Override
	public String getPrice() {
	
		return "LKR 6500000/-";
	}

	@Override
	public String getSpeed() {
	
		return "Acceleration (0-100 kmph)	12.36 Seconds";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return false;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {
	
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {
	
		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {
		
		return true;
	}

	@Override
	public String getadditionalfeatures() {
	
		return "\r\n"+" - Power Steering"+"\r\n"+" - Automatic Climate Control"+"\r\n"+" - Alloy Wheels"+"\r\n"+" - Fog Lights - Front"+"\r\n"+" - Navigation System"+
		"\r\n"+" - Height Adjustable Driver Seat";
	}

}
