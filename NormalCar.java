package bcas.ap.inter.car;

public interface NormalCar extends Car {

	public boolean isHeatedSeatandHeatedSteeringWheel();
	public boolean isDayandNightIRVM();
	public boolean isAirConditioning();
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth();
	public boolean isPowerWindows();
	public boolean isRearDoorChildLock();
	public boolean isABSsystem();
	public boolean isDualAirbags();
	public String getadditionalfeatures();
}
