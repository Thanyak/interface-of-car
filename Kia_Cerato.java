package bcas.ap.inter.car;

public class Kia_Cerato implements LuxuryCar{

	@Override
	public String carType() {

		return "Luxury Car";
	}

	@Override
	public String carBrand() {
	
		return "Kia";
	}

	@Override
	public String carModel() {

		return "Kia_Cerato";
	}

	@Override
	public String getColor() {

		return "Steel grey";
	}

	@Override
	public String getPrice() {

		return "Rs.5,650,000";
	}

	@Override
	public String getSpeed() {

		return "Acceleration from 0-100 km / h Kia Cerato II 6-speed ";
	}

	@Override
	public int getSeats() {
	
		return 5;
	}

	@Override
	public String isbodyConstruction() {
	
		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {
	
		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {
	
		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {
	
		return true;
	}

	@Override
	public boolean isABSsystem() {
	
		return true;
	}

	@Override
	public boolean isDualAirbags() {
	
		return true;
	}


	@Override
	public boolean isRoofWindow() {
	
		return true;
	}

	@Override
	public boolean isbackupCamera() {
	
		return true;
	}

	@Override
	public boolean isNavigationSystem() {
	
		return true;
	}

	@Override
	public boolean isRemoteStart() {
	
		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {
	
		return true;
	}

	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - Folding key"+"\r\n"+" - Anti-theft immobiliser"+"\r\n"+" - Impact sensing auto door unlocking"+"\r\n"+" - Burglar alarm with security indicator"+"\r\n";
	}
}
