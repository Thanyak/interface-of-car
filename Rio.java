package bcas.ap.inter.car;

public class Rio implements LuxuryCar{

	@Override
	public String carType() {

		return "Luxury Car";
	}

	@Override
	public String carBrand() {

		return "Kia";
	}

	@Override
	public String carModel() {

		return "Rio.";
	}

	@Override
	public String getColor() {

		return "Yas Marina Blue";
	}

	@Override
	public String getPrice() {
	
		return "Rs 10,300,000";
	}

	@Override
	public String getSpeed() {

		return "0-100 km/h - (sec):12.2";
	}

	@Override
	public int getSeats() {

		return 5;
	}

	@Override
	public String isbodyConstruction() {

		return "Super";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {

		return true;
	}

	@Override
	public boolean isPowerWindows() {

		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {

		return true;
	}

	@Override
	public boolean isDualAirbags() {

		return true;
	}

	@Override
	public boolean isRoofWindow() {

		return true;
	}

	@Override
	public boolean isbackupCamera() {

		return true;
	}

	@Override
	public boolean isNavigationSystem() {

		return true;
	}

	@Override
	public boolean isRemoteStart() {

		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return true;
	}

	@Override
	public String getadditionalfeatures() {

		return "\r\n"+" - Full-folding 2nd row seats"+"\r\n"+" - Convering Shel"+"\r\n"+" - Manual and Automatic Transmission"+"\r\n"+" - Parking Distance Warning-Reverse"+"\r\n"+
				" - Straight line Stability";
	}

}
