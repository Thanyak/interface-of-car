package bcas.ap.inter.car;

public class Kia implements LuxuryCar {

	@Override
	public String carType() {
	
		return "Luxury Car";
	}

	@Override
	public String carBrand() {
	
		return "KIA";
	}

	@Override
	public String carModel() {
	
		return "Picanto";
	}

	@Override
	public String getColor() {
	
		return "red";
	}

	@Override
	public String getPrice() {

		return "Rs. 3,150,000";
	}

	@Override
	public String getSpeed() {

		return " 1.0 to 62 mph in 14.3 seconds and on to a maximum speed of 100 mph. ";
	}

	@Override
	public int getSeats() {

		return 4;
	}

	@Override
	public String isbodyConstruction() {
	
		return "Good";
	}

	@Override
	public boolean isHeatedSeatandHeatedSteeringWheel() {

		return true;
	}

	@Override
	public boolean isDayandNightIRVM() {

		return true;
	}

	@Override
	public boolean isAirConditioning() {

		return true;
	}

	@Override
	public boolean isMusicSystemWithUSB_AUX_Radio_Bluetooth() {
	
		return true;
	}

	@Override
	public boolean isPowerWindows() {
	
		return true;
	}

	@Override
	public boolean isRearDoorChildLock() {

		return true;
	}

	@Override
	public boolean isABSsystem() {
	
		return true;
	}

	@Override
	public boolean isDualAirbags() {
	
		return true;
	}

	@Override
	public boolean isRoofWindow() {
	
		return false;
	}

	@Override
	public boolean isbackupCamera() {
	
		return true;
	}

	@Override
	public boolean isNavigationSystem() {
	
		return true;
	}

	@Override
	public boolean isRemoteStart() {

		return true;
	}

	@Override
	public boolean isautomaticEmergencyBreaking() {

		return true;
	}


	@Override
	public String getadditionalfeatures() {
		// TODO Auto-generated method stub
		return "\r\n"+" - The Kia Picanto has 1 Petrol Engine on offer"+ "\r\n"+" - The Petrol engine is cc."+"\r\n"+" - It is available with the Manual transmission"+
				"\r\n"+" - Anti-Theft Alarm System."+"\r\n"+" - Engine Immobiliser";
	}

	
}
