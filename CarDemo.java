package bcas.ap.inter.car;

public class CarDemo {

	public static void main(String[] args) {
		
		BMW car=new BMW();
		System.out.println("The car type is : "+car.carType()+ "\n" +"Brand of the car : "+ car.carBrand()+"\n" + "Model of the car : "+car.carModel()+"\n" +
		"Color of the car : "+ car.getColor()+"\n"+"Price of the car : "+car.getPrice()+"\n" +"Speed of the car : "+ car.getSpeed()+"\n" + "No of seats : " +
		car.getSeats()+"\n" +"Body Construction : "+ car.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car.isDualAirbags()+"\n" +"It has roof windows: "+ car.isRoofWindow()+"\n" +"It has backup camera : "+car.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car.isNavigationSystem()+"\n" +"It has Remort start facility : "+car.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car.getadditionalfeatures());
		
		System.out.println(                                                                                                                 );
		
		
		MercedesBenz car2=new MercedesBenz();
		System.out.println("The car type is : "+car2.carType()+ "\n" +"Brand of the car : "+ car2.carBrand()+"\n" + "Model of the car : "+car2.carModel()+"\n" +
		"Color of the car : "+ car2.getColor()+"\n"+"Price of the car : "+car2.getPrice()+"\n" +"Speed of the car : "+ car2.getSpeed()+"\n" + "No of seats : " +
		car2.getSeats()+"\n" +"Body Construction : "+ car2.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car2.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car2.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car2.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car2.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car2.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car2.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car2.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car2.isDualAirbags()+"\n" +"It has roof windows: "+ car2.isRoofWindow()+"\n" +"It has backup camera : "+car2.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car2.isNavigationSystem()+"\n" +"It has Remort start facility : "+car2.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car2.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car2.getadditionalfeatures());
		
		System.out.println(                                                                                                                 );
		
		Toyota car3=new Toyota();
		System.out.println("The car type is : "+car3.carType()+ "\n" +"Brand of the car : "+ car3.carBrand()+"\n" + "Model of the car : "+car3.carModel()+"\n" +
		"Color of the car : "+ car3.getColor()+"\n"+"Price of the car : "+car3.getPrice()+"\n" +"Speed of the car : "+ car3.getSpeed()+"\n" + "No of seats : " +
		car3.getSeats()+"\n" +"Body Construction : "+ car3.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car3.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car3.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car3.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car3.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car3.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car3.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car3.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car3.isDualAirbags()+"Further Information: "+car3.getadditionalfeatures());
		
		System.out.println(                                                                                                                 );
	
		Kia car4=new Kia();
		System.out.println("The car type is : "+car4.carType()+ "\n" +"Brand of the car : "+ car4.carBrand()+"\n" + "Model of the car : "+car4.carModel()+"\n" +
		"Color of the car : "+ car4.getColor()+"\n"+"Price of the car : "+car4.getPrice()+"\n" +"Speed of the car : "+ car4.getSpeed()+"\n" + "No of seats : " +
		car4.getSeats()+"\n" +"Body Construction : "+ car4.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car4.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car4.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car4.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car4.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car4.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car4.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car4.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car4.isDualAirbags()+"\n" +"It has roof windows: "+ car4.isRoofWindow()+"\n" +"It has backup camera : "+car4.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car4.isNavigationSystem()+"\n" +"It has Remort start facility : "+car4.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car4.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car4.getadditionalfeatures());
	
		
		System.out.println(                                                                                                                 );
		
		Audi car5=new Audi();
		System.out.println("The car type is : "+car5.carType()+ "\n" +"Brand of the car : "+ car5.carBrand()+"\n" + "Model of the car : "+car5.carModel()+"\n" +
		"Color of the car : "+ car5.getColor()+"\n"+"Price of the car : "+car5.getPrice()+"\n" +"Speed of the car : "+ car5.getSpeed()+"\n" + "No of seats : " +
		car5.getSeats()+"\n" +"Body Construction : "+ car5.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car5.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car5.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car5.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car5.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car5.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car5.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car5.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car5.isDualAirbags()+"\n" +"It has roof windows: "+ car5.isRoofWindow()+"\n" +"It has backup camera : "+car5.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car5.isNavigationSystem()+"\n" +"It has Remort start facility : "+car5.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car5.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car5.getadditionalfeatures());
		
		System.out.println(                                                                                                                 );
	
		MarutiSwift car6=new MarutiSwift();
		System.out.println("The car type is : "+car6.carType()+ "\n" +"Brand of the car : "+ car6.carBrand()+"\n" + "Model of the car : "+car6.carModel()+"\n" +
		"Color of the car : "+ car6.getColor()+"\n"+"Price of the car : "+car6.getPrice()+"\n" +"Speed of the car : "+ car6.getSpeed()+"\n" + "No of seats : " +
		car6.getSeats()+"\n" +"Body Construction : "+ car6.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car6.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car6.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car6.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car6.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car6.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car6.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car6.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car6.isDualAirbags()+"\n" +"Further information: "+car6.getadditionalfeatures());
		
		System.out.println(                                                                                                                 );
	
		MercedesBenzGLASUV car7=new MercedesBenzGLASUV();
		System.out.println("The car type is : "+car7.carType()+ "\n" +"Brand of the car : "+ car7.carBrand()+"\n" + "Model of the car : "+car7.carModel()+"\n" +
		"Color of the car : "+ car7.getColor()+"\n"+"Price of the car : "+car7.getPrice()+"\n" +"Speed of the car : "+ car7.getSpeed()+"\n" + "No of seats : " +
		car7.getSeats()+"\n" +"Body Construction : "+ car7.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car7.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car7.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car7.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car7.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car7.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car7.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car7.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car7.isDualAirbags()+"\n" +"It has roof windows: "+ car7.isRoofWindow()+"\n" +"It has backup camera : "+car7.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car7.isNavigationSystem()+"\n" +"It has Remort start facility : "+car7.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car7.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car7.getadditionalfeatures());
	
		
		System.out.println(                                                                                                                 );
		
		Suzuki car8=new Suzuki();
		System.out.println("The car type is : "+car8.carType()+ "\n" +"Brand of the car : "+ car8.carBrand()+"\n" + "Model of the car : "+car8.carModel()+"\n" +
		"Color of the car : "+ car8.getColor()+"\n"+"Price of the car : "+car8.getPrice()+"\n" +"Speed of the car : "+ car8.getSpeed()+"\n" + "No of seats : " +
		car8.getSeats()+"\n" +"Body Construction : "+ car8.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car8.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car8.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car8.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car8.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car8.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car8.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car8.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car8.isDualAirbags()+"\n" +"Further information: "+car8.getadditionalfeatures());
		
		System.out.println(                                                                                                                 );
		
	
		BMW_M car9=new BMW_M();
		System.out.println("The car type is : "+car9.carType()+ "\n" +"Brand of the car : "+ car9.carBrand()+"\n" + "Model of the car : "+car9.carModel()+"\n" +
		"Color of the car : "+ car9.getColor()+"\n"+"Price of the car : "+car9.getPrice()+"\n" +"Speed of the car : "+ car9.getSpeed()+"\n" + "No of seats : " +
		car9.getSeats()+"\n" +"Body Construction : "+ car9.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car9.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car9.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car9.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car9.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car9.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car9.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car9.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car9.isDualAirbags()+"\n" +"It has roof windows: "+ car9.isRoofWindow()+"\n" +"It has backup camera : "+car9.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car9.isNavigationSystem()+"\n" +"It has Remort start facility : "+car9.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car9.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car9.getadditionalfeatures());
		
		System.out.println(                                                                                                                 );
	
		Rio car10=new Rio();
		System.out.println("The car type is : "+car10.carType()+ "\n" +"Brand of the car : "+ car10.carBrand()+"\n" + "Model of the car : "+car10.carModel()+"\n" +
		"Color of the car : "+ car10.getColor()+"\n"+"Price of the car : "+car10.getPrice()+"\n" +"Speed of the car : "+ car10.getSpeed()+"\n" + "No of seats : " +
		car10.getSeats()+"\n" +"Body Construction : "+ car10.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car10.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car10.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car10.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car10.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car10.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car10.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car10.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car10.isDualAirbags()+"\n" +"It has roof windows: "+ car10.isRoofWindow()+"\n" +"It has backup camera : "+car10.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car10.isNavigationSystem()+"\n" +"It has Remort start facility : "+car10.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car10.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car10.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );
	
		Audi_A8 car11=new Audi_A8();
		System.out.println("The car type is : "+car11.carType()+ "\n" +"Brand of the car : "+ car11.carBrand()+"\n" + "Model of the car : "+car11.carModel()+"\n" +
		"Color of the car : "+ car11.getColor()+"\n"+"Price of the car : "+car11.getPrice()+"\n" +"Speed of the car : "+ car11.getSpeed()+"\n" + "No of seats : " +
		car11.getSeats()+"\n" +"Body Construction : "+ car11.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car11.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car11.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car11.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car11.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car11.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car11.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car11.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car11.isDualAirbags()+"\n" +"It has roof windows: "+ car11.isRoofWindow()+"\n" +"It has backup camera : "+car11.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car11.isNavigationSystem()+"\n" +"It has Remort start facility : "+car11.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car11.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car11.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );

	
		Toyota_Camry car12=new Toyota_Camry();
		System.out.println("The car type is : "+car12.carType()+ "\n" +"Brand of the car : "+ car12.carBrand()+"\n" + "Model of the car : "+car12.carModel()+"\n" +
		"Color of the car : "+ car12.getColor()+"\n"+"Price of the car : "+car12.getPrice()+"\n" +"Speed of the car : "+ car12.getSpeed()+"\n" + "No of seats : " +
		car12.getSeats()+"\n" +"Body Construction : "+ car12.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car12.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car12.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car12.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car12.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car12.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car12.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car12.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car12.isDualAirbags()+"\n" + "Further Information: "+car12.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );

	
		SuzukiAlto car13=new SuzukiAlto();
		System.out.println("The car type is : "+car13.carType()+ "\n" +"Brand of the car : "+ car13.carBrand()+"\n" + "Model of the car : "+car13.carModel()+"\n" +
		"Color of the car : "+ car13.getColor()+"\n"+"Price of the car : "+car13.getPrice()+"\n" +"Speed of the car : "+ car13.getSpeed()+"\n" + "No of seats : " +
		car13.getSeats()+"\n" +"Body Construction : "+ car13.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car13.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car13.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car13.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car13.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car13.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car13.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car13.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car13.isDualAirbags()+"\n" + "Further Information: "+car13.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );
	
		
		Maruti_Vitara car14=new Maruti_Vitara();
		System.out.println("The car type is : "+car14.carType()+ "\n" +"Brand of the car : "+ car14.carBrand()+"\n" + "Model of the car : "+car14.carModel()+"\n" +
		"Color of the car : "+ car14.getColor()+"\n"+"Price of the car : "+car14.getPrice()+"\n" +"Speed of the car : "+ car14.getSpeed()+"\n" + "No of seats : " +
		car14.getSeats()+"\n" +"Body Construction : "+ car14.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car14.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car14.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car14.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car14.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car14.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car14.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car14.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car14.isDualAirbags()+"\n" + "Further Information: "+car14.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );
	
		Audi_Q7 car15=new Audi_Q7();
		System.out.println("The car type is : "+car15.carType()+ "\n" +"Brand of the car : "+ car15.carBrand()+"\n" + "Model of the car : "+car15.carModel()+"\n" +
		"Color of the car : "+ car15.getColor()+"\n"+"Price of the car : "+car15.getPrice()+"\n" +"Speed of the car : "+ car15.getSpeed()+"\n" + "No of seats : " +
		car15.getSeats()+"\n" +"Body Construction : "+ car15.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car15.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car15.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car15.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car15.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car15.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car15.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car15.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car15.isDualAirbags()+"\n" +"It has roof windows: "+ car15.isRoofWindow()+"\n" +"It has backup camera : "+car15.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car15.isNavigationSystem()+"\n" +"It has Remort start facility : "+car15.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car15.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car15.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );
	
		Benz_GLB_SUV car16=new Benz_GLB_SUV();
		System.out.println("The car type is : "+car16.carType()+ "\n" +"Brand of the car : "+ car16.carBrand()+"\n" + "Model of the car : "+car16.carModel()+"\n" +
		"Color of the car : "+ car16.getColor()+"\n"+"Price of the car : "+car16.getPrice()+"\n" +"Speed of the car : "+ car16.getSpeed()+"\n" + "No of seats : " +
		car16.getSeats()+"\n" +"Body Construction : "+ car16.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car16.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car16.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car16.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car16.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car16.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car16.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car16.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car16.isDualAirbags()+"\n" +"It has roof windows: "+ car16.isRoofWindow()+"\n" +"It has backup camera : "+car16.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car16.isNavigationSystem()+"\n" +"It has Remort start facility : "+car16.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car16.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car16.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );
	
		BMW_Mini_Cooper car17=new BMW_Mini_Cooper();
		System.out.println("The car type is : "+car17.carType()+ "\n" +"Brand of the car : "+ car17.carBrand()+"\n" + "Model of the car : "+car17.carModel()+"\n" +
		"Color of the car : "+ car17.getColor()+"\n"+"Price of the car : "+car17.getPrice()+"\n" +"Speed of the car : "+ car17.getSpeed()+"\n" + "No of seats : " +
		car17.getSeats()+"\n" +"Body Construction : "+ car17.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car17.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car17.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car17.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car17.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car17.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car17.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car17.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car17.isDualAirbags()+"\n" +"It has roof windows: "+ car17.isRoofWindow()+"\n" +"It has backup camera : "+car17.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car17.isNavigationSystem()+"\n" +"It has Remort start facility : "+car17.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car17.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car17.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );
	
		Kia_Cerato car18=new Kia_Cerato();
		System.out.println("The car type is : "+car18.carType()+ "\n" +"Brand of the car : "+ car18.carBrand()+"\n" + "Model of the car : "+car18.carModel()+"\n" +
		"Color of the car : "+ car18.getColor()+"\n"+"Price of the car : "+car18.getPrice()+"\n" +"Speed of the car : "+ car18.getSpeed()+"\n" + "No of seats : " +
		car18.getSeats()+"\n" +"Body Construction : "+ car18.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car18.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car18.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car18.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car18.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car18.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car18.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car18.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car18.isDualAirbags()+"\n" +"It has roof windows: "+ car18.isRoofWindow()+"\n" +"It has backup camera : "+car18.isbackupCamera()+
		"\n" + "It has Navigation system : "+ car18.isNavigationSystem()+"\n" +"It has Remort start facility : "+car18.isRemoteStart()+"\n" +
		"It has Automatic Emergency breaking : "+car18.isautomaticEmergencyBreaking()+"\n" +"Further information: "+car18.getadditionalfeatures());
		
		System.out.println(                                                                                                                  );
	
		Toyota_86 car19=new Toyota_86();
		System.out.println("The car type is : "+car19.carType()+ "\n" +"Brand of the car : "+ car19.carBrand()+"\n" + "Model of the car : "+car19.carModel()+"\n" +
		"Color of the car : "+ car19.getColor()+"\n"+"Price of the car : "+car19.getPrice()+"\n" +"Speed of the car : "+ car19.getSpeed()+"\n" + "No of seats : " +
		car19.getSeats()+"\n" +"Body Construction : "+ car19.isbodyConstruction()+"\n"+ "It has heated seat & heated steering Wheel : "+ 
		car19.isHeatedSeatandHeatedSteeringWheel()+"\n" + "It has day and night IVRM : "+car19.isDayandNightIRVM()+"\n" +"It consists Air conditioning : "+
		car19.isAirConditioning()+"\n" +"It has music system with USB,AUX,Radio & Bluetooth facilities :"+car19.isMusicSystemWithUSB_AUX_Radio_Bluetooth()+"\n"+
		"It has power windows : "+car19.isPowerWindows()+"\n" + "It has Rear Door child lock : "+car19.isRearDoorChildLock()+"\n" +"It has ABS System:"+ car19.isABSsystem()+"\n" + 
		"It has dual air bags : "+ car19.isDualAirbags()+"\n"+"Further information: "+car19.getadditionalfeatures());
	}
	
	
	
	

}